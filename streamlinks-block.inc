<?php

 drupal_add_js($javascript)

 $javascript = <<<JAVASCRIPT
   <script type="text/javascript">

   //JK Popup Window Script (version 3.0)- By JavaScript Kit (http://www.javascriptkit.com)
   //Visit JavaScriptKit.com for free JavaScripts
   //This notice must stay intact for legal use
  
   function openpopup(popurl){
     var winpops=window.open(popurl,"mnnpopup","width=640,height=480")
   }
   </script>
 JAVASCRIPT;

   $output = <<<EOT
   <ul class="menu">
     <li class="leaf"><b>MNN1 <span class="greenpop">Community</span></b> (34/82/33)&nbsp;
       <a href="javascript:openpopup('http://mnn1.mnntv.org/OnAir/flash/live.html')">Watch MNN1 Live</a><br />
     </li>
     <li class="leaf"><b>MNN2<span class="greenpop"> <br />Lifestyle</span></b> (56/83/34)&nbsp;
       <a href="javascript:openpopup('http://mnn2.mnntv.org/OnAir/flash')">Watch MNN2 Live</a><br />
     </li>
     <li class="leaf"><b>MNN3<span class="greenpop"> <br />Spirit</span></b> (57/84/35)&nbsp;
       <a href="javascript:openpopup('http://mnn3.mnntv.org/OnAir/flash')">Watch MNN3 Live</a><br />
     </li>
     <li class="leaf"><b>MNN4<span class="greenpop"> <br />Culture</span></b> (67/85/36)&nbsp;
       <a href="javascript:openpopup('http://mnn4.mnntv.org/OnAir/flash')">Watch MNN4 Live</a><br />
     </li>
   </ul>
   <div align="right"><a href="/help/webstreams">Need Help?</a></div>
 EOT;
