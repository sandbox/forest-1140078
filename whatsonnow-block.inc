<?php
// file: whatsonnow-block.inc
/**
 * Created on Jun 11, 2009
 *
 * Name: What's On
 * Author: Jacob Redding Forest Mars et al
 * Version: 0.5
 * Purpose: To display what is playing right now
 */

//Constants

 $error = "DB: Temporarily offline,\n".
                     "The Schedule will return shortly";
 error_reporting(1);

//Connect to DB
 $dbsocket = mysql_connect('216.164.83.189', 'mnnwebsite', '6ngiyS9h') or die($error);
 mysql_select_db('mnnlive') or die('Could not select database');

//  Don't show this here, b/c the black is already outputting a title
// $output .= '<h3>On Now...</h3>';

//Before we build our Queries (four in total) we need an array of the channels to loop through

//mjg: change this
 $channels = array("Channel 1","Channel 2","Channel 3","Channel 4");
 $time = time();
 $time_hr = time() - 16800;
 $time_before = time() - 21600;
 $time_after = time() - 18000;
 foreach ($channels as $channel) {

    //Build the Query
/**   $query = "SELECT n2.title FROM node n2 " .
              "LEFT JOIN relativity rel ON rel.parent_nid=n2.nid " .
              "LEFT JOIN relativity rel2 on rel.nid=rel2.parent_nid " .
              "LEFT JOIN node n ON n.nid=rel2.nid " .
              "RIGHT JOIN node_content_airing nca ON n.nid=nca.nid " .
              "LEFT JOIN event e ON e.nid=n.nid " .
              "LEFT JOIN term_node tn ON tn.nid=n.nid " .
              "LEFT JOIN term_data td ON tn.tid=td.tid " .
              "WHERE td.name = '".$channel."'" .
              " AND e.event_start > ".$time_before." AND e.event_start < ".time().
              " ORDER BY e.event_start ";
*/
   $query = "SELECT n.title". //, e.event_start ".
            " FROM node n ".
            "  JOIN event e ON e.nid=n.nid ".
            "  JOIN term_node tn ON tn.nid=n.nid ".
            "  JOIN term_data td ON tn.tid=td.tid ".
            " WHERE td.name = '".$channel."'".
            "  AND e.event_start < '".$time."' ".
            " ORDER BY e.event_start desc ".
            "LIMIT 1";
   $result=mysql_query($query);
 // Execute the Query
   $schedule_results = mysql_fetch_row($result);
 // Grab the Title
   $show_title = $schedule_results[0];
 // find where to chop the title, we don't need Episode # and date
   $pos = strpos($show_title, 'Ep-');
 // choppy-chop-chop
   $show_title = substr($show_title, 0, $pos);

 // insert elipses if the title is too long
   if (strlen($show_title) > 20) {
     $show_title_short = substr($show_title,0,20) . "...";
   } else {
     $show_title_short = $show_title;
   }
 // fix the channel number string
   $class = "";
   $channel_to_display = substr($channel,-2);
   if ($channel_to_display == " 2" | $channel_to_display == " 4") { $class = " even"; } else { $class = " odd"; }
    
   $output .= "<div class=\"".$class."\" title=\"$show_title\"><strong>". $channel_to_display ."</strong> ". $show_title_short ."</div>\n";
}

mysql_close($dbsocket);

$output .= '<div><a href="/viewers/whatsonnow">more...</a></div>';
$output .= '<div><a href="/viewers/schedule">daily schedule...</a></div>';

$dat = date("m/d/y/m:h:s",$time_hr);
#$output .= "<div class=\"".$class."\" title=\"$show_title\"><strong>". "</strong> ". $dat ."</div>\n";
